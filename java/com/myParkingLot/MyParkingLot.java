package com.myParkingLot;

import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;
import com.myParkingLot.model.Cars;

public class MyParkingLot {
	
	public static void main (String[] args) {
		Cars cars = new Cars();
		try {
			Scanner in = new Scanner(System.in);
			
			System.out.println("Please add one of the below commands, {} value has to be added as per your requirement.");
			System.out.println("To give size for your parking lot  ->  create_parking_lot {capacity}");
			System.out.println("To park the car  ->  park {car_number} {car_color}");
			System.out.println("Unpark the car from the lot  ->  leave {slot_number}");
			System.out.println("Print the status of the parking lot  ->  status");
			System.out.println("Get cars registration number for the given car color  ->  registration_numbers_for_cars_with_color {car_color}");
			System.out.println("Get slot numbers for the given car color  ->  slot_numbers_for_cars_with_color {car_color}");
			System.out.println("Get slot number for the given car number  ->  slot_number_for_registration_number {car_number}");
			
			System.out.println("Please Enter 'exit' to end execution");
			System.out.println("Input : ");
			String input = in.nextLine();
			String[] inputs = input.split(" ");
			String key = inputs[0];
			SortedMap<Integer, Cars> map = null;
			Integer slotNumber = 1;
			int capacity = 0;
			switch(key) {
			case "create_parking_lot":
				capacity = Integer.parseInt(inputs[1]);
				map = new TreeMap<Integer, Cars>();
				System.out.println(map);
				break;
			case "park":
				if (map.size() < capacity) {
				cars.setSlot_number(slotNumber);
				cars.setCar_number(inputs[1]);
				cars.setCar_color(inputs[2]);
				map.put(slotNumber++, cars);
				}
				
			default:
				System.out.println("Invalid");
			}
			
			
			
		} catch (Exception e) {
			System.out.printf("Exception : {}",e.getMessage());
		}
	}

}
