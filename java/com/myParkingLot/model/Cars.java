package com.myParkingLot.model;

public class Cars {
	private Integer slot_number;
	private String car_number;
	private String car_color;
	
	public Integer getSlot_number() {
		return slot_number;
	}
	public void setSlot_number(Integer slot_number) {
		this.slot_number = slot_number;
	}
	public String getCar_number() {
		return car_number;
	}
	public void setCar_number(String car_number) {
		this.car_number = car_number;
	}
	public String getCar_color() {
		return car_color;
	}
	public void setCar_color(String car_color) {
		this.car_color = car_color;
	}
}
