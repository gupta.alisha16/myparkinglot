package com.myParkingLot.Test;

import java.util.SortedMap;
import java.util.TreeMap;

import com.myParkingLot.Service.Logic;
import com.myParkingLot.model.Cars;

import junit.framework.TestCase;

public class MyParkingLotTest extends TestCase{

	SortedMap<Integer, Cars> map = null;
	
	public void setUp() {
		map = new TreeMap<Integer, Cars>();
	}
	
	public void testPark() throws Exception {
		
		String output = Logic.park(map, 4, "CH-01A-1111", "White");
		Logic.leave(map, "1");
		assertEquals("\nAllocated slot number: 1\n", output);
	}
	
	public void testLeave() throws Exception {
		
		Logic.park(map, 4, "CH-01A-1111", "White");
		String output = Logic.leave(map, "1");
		assertEquals("\nSlot number 1 is free\n", output);
	}
	
	public void testRegistrationNumbersForCarsWithColour() throws Exception {
		Logic.park(map, 4, "CH-01A-1111", "White");
		Logic.park(map, 4, "CH-01A-2222", "White");
		Logic.park(map, 4, "CH-01A-3333", "Black");
		String output = Logic.registrationNumbersForCarsWithColour(map, "White");
		Logic.leave(map, "1");
		Logic.leave(map, "2");
		Logic.leave(map, "3");
		assertEquals("\nCH-01A-1111, CH-01A-2222\n", output);
	}
	
	public void testSlotNumbersForCarsWithColour() throws Exception {
		Logic.park(map, 4, "CH-01A-1111", "White");
		Logic.park(map, 4, "CH-01A-2222", "White");
		Logic.park(map, 4, "CH-01A-3333", "Black");
		String output = Logic.slot_numbers_for_cars_with_colour(map, "Black");
		Logic.leave(map, "1");
		Logic.leave(map, "2");
		Logic.leave(map, "3");
		assertEquals("\n3\n", output);
	}
	
	public void testSlotNumberForRegistrationNumber() throws Exception {
		Logic.park(map, 4, "CH-01A-1111", "White");
		Logic.park(map, 4, "CH-01A-2222", "White");
		Logic.park(map, 4, "CH-01A-3333", "Black");
		String output = Logic.slot_number_for_registration_number(map, "CH-01A-2222");
		Logic.leave(map, "1");
		Logic.leave(map, "2");
		Logic.leave(map, "3");
		assertEquals("\n2\n", output);
	}
	

}

