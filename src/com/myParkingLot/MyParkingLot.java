package com.myParkingLot;

import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

import com.myParkingLot.Service.Logic;
import com.myParkingLot.model.Cars;

public class MyParkingLot {
	
	public static void main (String[] args) {
		
		SortedMap<Integer, Cars> map = null;
		int capacity = 0;

		try {
			String input = "";
			do{
			printInfo();
			Scanner in = new Scanner(System.in);
			input = in.nextLine();
			String[] inputs = input.split(" ");
			String key = inputs[0];
			
			switch(key) {
			case "create_parking_lot":
				capacity = Integer.parseInt(inputs[1]);
				map = new TreeMap<Integer, Cars>();
				System.out.println("\nCreated a parking lot with " + capacity + " lots\n");
				break;
			case "park":
				System.out.println(Logic.park(map, capacity, inputs[1], inputs[2]));
				break;
			case "leave":
				System.out.println(Logic.leave(map, inputs[1]));
				break;
			case "status":
				Logic.status(map);
				break;
			case "registration_numbers_for_cars_with_colour":
				System.out.println(Logic.registrationNumbersForCarsWithColour(map, inputs[1]));
				break;
			case "slot_numbers_for_cars_with_colour":
				System.out.println(Logic.slot_numbers_for_cars_with_colour(map, inputs[1]));
				break;
			case "slot_number_for_registration_number":
				System.out.println(Logic.slot_number_for_registration_number(map, inputs[1]));
				break;
			default:
				System.out.println("\nInvalid Input\n");
			}
			
			} while(!input.equalsIgnoreCase("exit"));
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	public static void printInfo() {
		System.out.println("Please add one of the below commands, {} value has to be added as per your requirement.");
		System.out.println("To give size for your parking lot  ->  create_parking_lot {capacity}");
		System.out.println("To park the car  ->  park {car_number} {car_color}");
		System.out.println("Unpark the car from the lot  ->  leave {slot_number}");
		System.out.println("Print the status of the parking lot  ->  status");
		System.out.println("Get cars registration number for the given car color  ->  registration_numbers_for_cars_with_colour {car_color}");
		System.out.println("Get slot numbers for the given car color  ->  slot_numbers_for_cars_with_colour {car_color}");
		System.out.println("Get slot number for the given car number  ->  slot_number_for_registration_number {car_number}");
		
		System.out.println("Please Enter 'exit' to end execution");
		System.out.println("Input : ");
	}
}
