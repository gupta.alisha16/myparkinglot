package com.myParkingLot.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.stream.Collectors;

import com.myParkingLot.model.Cars;

public class Logic {

	public static String park(SortedMap<Integer, Cars> map, int capacity, String carNumber, String carColor) {
		int slotNumber = 1;
		if (map.size() < capacity) {
			if (map.size() != 0) {

				ArrayList<Integer> keySet = new ArrayList<Integer>(map.keySet());
				int l = 1, h = keySet.size();
				while (l <= h) {
					if (l != keySet.get(l - 1)) {
						slotNumber = l;
						break;
					}
					l++;
					slotNumber = l;
				}
			}
			Cars cars = new Cars();
			cars.setSlot_number(slotNumber);
			cars.setCar_number(carNumber);
			cars.setCar_color(carColor);
			map.put(slotNumber, cars);
			return "\nAllocated slot number: " + slotNumber + "\n";
		} else {
			return "\nSorry, parking lot is full\n";
		}
	}

	public static String leave(SortedMap<Integer, Cars> map, String slotNumber) {
		if (map.get(Integer.parseInt(slotNumber)) != null) {
			map.remove(Integer.parseInt(slotNumber));
			return "\nSlot number " + Integer.parseInt(slotNumber) + " is free\n";
		} else {
			return "\nNot Found\n";
		}
	}

	public static void status(SortedMap<Integer, Cars> map) {
		System.out.println("\nSlot No. \t Registration No \t Color");
		for (Map.Entry<Integer, Cars> entry : map.entrySet()) {
			System.out.println(entry.getValue().getSlot_number() + " \t\t " + entry.getValue().getCar_number()
					+ " \t\t " + entry.getValue().getCar_color());
		}
	}

	public static String registrationNumbersForCarsWithColour(SortedMap<Integer, Cars> map, String carColor) {
		boolean flagForCarNumber = false;
		List<String> listForCarNumbers = new ArrayList<String>();
		String carNumbersForColors = "";

		for (Map.Entry<Integer, Cars> entry : map.entrySet()) {
			if (carColor.equalsIgnoreCase(entry.getValue().getCar_color())) {
				flagForCarNumber = true;
				listForCarNumbers.add(entry.getValue().getCar_number());
			}
		}

		if (flagForCarNumber == false)
			return "\nNot found\n";
		else {
			carNumbersForColors = String.join(", ", listForCarNumbers);
			return "\n" + carNumbersForColors + "\n";
		}
	}

	public static String slot_numbers_for_cars_with_colour(SortedMap<Integer, Cars> map, String carColor) {
		boolean flagForSlots = false;
		List<Integer> listForSlots = new ArrayList<Integer>();
		String slotsForColors = "";

		for (Map.Entry<Integer, Cars> entry : map.entrySet()) {
			if (carColor.equalsIgnoreCase(entry.getValue().getCar_color())) {
				flagForSlots = true;
				listForSlots.add(entry.getValue().getSlot_number());
			}
		}

		if (flagForSlots == false)
			return "\nNot found\n";
		else {
			slotsForColors = listForSlots.stream().map(String::valueOf).collect(Collectors.joining(", "));
			return "\n" + slotsForColors + "\n";
		}
	}

	public static String slot_number_for_registration_number(SortedMap<Integer, Cars> map, String carNumber) {

		String slotNo = null;

		for (Map.Entry<Integer, Cars> entry : map.entrySet()) {
			if (carNumber.equalsIgnoreCase(entry.getValue().getCar_number())) {
				slotNo = "\n" + entry.getValue().getSlot_number() + "\n";
			}
		}

		if (slotNo == null) {
			return "\nNot found\n";
		} else {
			return slotNo;
		}
	}

}
